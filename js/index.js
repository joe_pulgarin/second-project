
    $(function () {
        $("[data-toggle='tooltip']").tooltip();
        $("[data-toggle='popover']").popover();
        $('.carousel').carousel({
            interval: 3000
        });

        $('#exampleModal').on('show.bs.modal', function (e) {
            console.log('el modal contacto se está mostrando');

            $('#contactoBtn').removeClass(' btn-outline-secondary');
            $('#contactoBtn').addClass('btn-primary');
            $('#contactoBtn').prop('disabled', true);
        });
        $('#exampleModal').on('shown.bs.modal', function (e) {
            console.log('el modal contacto se está mostró');
        });
        $('#exampleModal').on('hide.bs.modal', function (e) {
            console.log('el modal contacto se está cerrando');

            $('#contactoBtn').removeClass(' btn-primary');
            $('#contactoBtn').addClass('btn-outline-secondary');
            $('#contactoBtn').prop('disabled', false);
        });
        $('#exampleModal').on('hidden.bs.modal', function (e) {
            console.log('el modal contacto se cerró');
        });

    }); 
